%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 程序说明：Kalman滤波用于温度测量的实例
% 详细原理介绍及中文注释请参考：
% 《卡尔曼滤波原理及应用-MATLAB仿真》，电子工业出版社，黄小平著。
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function main
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N=120; %采样点的个数 时间单位为分钟
CON=25; %室内温度的理论值
% 对状态和测量的初始化
Xexpect=CON*ones(1,N);%期望温度为恒定的25℃
X=zeros(1,N);  %房间各时刻真实温度值
Xkf=zeros(1,N);%Kalman滤波处理的状态 也叫估计值
Z=zeros(1,N); %温度计的测量值
P=zeros(1,N); 
%赋初值
X(1)=25.1; %假设初始值房间温度为25.1℃
P(1)=0.01; %初始值的协方差
Z(1)=24.9;
Xkf(1)=Z(1); %初始测量值为24.9 同时作为滤波器的初始估计状态
%噪声
Q=0.01;
R=0.25;
W=sqrt(Q)*randn(1,N); %噪声方差
V=sqrt(R)*randn(1,N); %噪声方差
%系统矩阵
F=1;
G=1;
H=1;
I=eye(1);  %本系统状态为一维
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=2:N
    %  第一步 随着时间的推移，房间真实温度波动变化
    X(k)=F*X(k-1)+G*W(k-1);
    %  第二步 随时间的推移，获取实时数据
    % Kalman滤波的目的 最大限度降低测量噪声R的影响，使得Xkf尽可能的逼近X
    Z(k)=H*X(k)+V(k);
    % Kalman滤波 注意对照公式推导
    X_pre=F*Xkf(k-1);           %状态预测
    P_pre=F*P(k-1)*F'+Q;        %协方差预测
    Kg=P_pre*inv(H*P_pre*H'+R); %计算Kalman增益
    e=Z(k)-H*X_pre;             %新息
    Xkf(k)=X_pre+Kg*e;          %状态更新
    P(k)=(I-Kg*H)*P_pre;        %协方差更新
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%计算误差
Err_Messure=zeros(1,N); %测量值与真实值之间的偏差
Err_Kalman=zeros(1,N);  %Kalman估计与真实值之间的偏差
for k=1:N
    Err_Messure(k)=abs(Z(k)-X(k));
    Err_Kalman(k)=abs(Xkf(k)-X(k));
end
t=1:N;
% 画图显示 依次输出 理论值 叠加过程噪声的真实值
figure('Name','Kalman Filter Simulation','NumberTitle','off');
plot(t,Xexpect,'-b',t,X,'-r',t,Z,'-k',t,Xkf,'-g');
legend('expected','real','measure','kalman extimate');         
xlabel('sample time');
ylabel('temperature');
title('Kalman Filter Simulation');
% 误差分析图 测量误差 Kalman滤波误差
figure('Name','Error Analysis','NumberTitle','off');
plot(t,Err_Messure,'-b',t,Err_Kalman,'-k');
legend('messure error','kalman error');         
xlabel('sample time');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

