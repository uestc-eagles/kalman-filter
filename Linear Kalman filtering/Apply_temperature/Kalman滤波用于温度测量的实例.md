# Kalman滤波用于温度测量的实例

## 原理介绍

研究对象为一个房间的温度用温度计测量房间的温度，假设温度计存在测量误差，第*k*次测量的数据不是100%准确。选择Kalman滤波算法根据第*k-1*时刻的温度值来预测*k*时刻的温度。

## 初始值设定与假设

房间期望的温度恒定为25℃，初始温度计测量值为24.9℃，初始房间实际温度为25.1℃

```matlab
N=120; %采样点的个数 时间单位为分钟
CON=25; %室内温度的理论值
% 对状态和测量的初始化
Xexpect=CON*ones(1,N);%期望温度为恒定的25℃
X=zeros(1,N);  %房间各时刻真实温度值
Xkf=zeros(1,N);%Kalman滤波处理的状态 也叫估计值
Z=zeros(1,N); %温度计的测量值
P=zeros(1,N); 
%赋初值
X(1)=25.1; %假设初始值房间温度为25.1℃
P(1)=0.01; %初始值的协方差
Z(1)=24.9;
Xkf(1)=Z(1); %初始测量值为24.9 同时作为滤波器的初始估计状态
%噪声
Q=0.01;
R=0.25;
W=sqrt(Q)*randn(1,N); %噪声方差
V=sqrt(R)*randn(1,N); %噪声方差
%系统矩阵
F=1;
G=1;
H=1;
I=eye(1);  %本系统状态为一维
```


## 第一步： 随着时间推移，房间真实温度波动变化的模型

```matlab
X(k)=F*X(k-1)+G*W(k-1);
```

## 第二步： 随时间的推移，获取实时数据

Kalman滤波的目的 最大限度降低测量噪声R的影响，使得Xkf尽可能的逼近X

```matlab
 Z(k)=H*X(k)+V(k);
```

## 第三步：应用 Kalman滤波 注意对照公式推导

```matlab
X_pre=F*Xkf(k-1);           %状态预测
P_pre=F*P(k-1)*F'+Q;        %协方差预测
Kg=P_pre*inv(H*P_pre*H'+R); %计算Kalman增益
e=Z(k)-H*X_pre;             %新息
Xkf(k)=X_pre+Kg*e;          %状态更新
P(k)=(I-Kg*H)*P_pre;        %协方差更新
```

## 第四步：计算误差并画图

```matlab
Err_Messure=zeros(1,N); %测量值与真实值之间的偏差
Err_Kalman=zeros(1,N);  %Kalman估计与真实值之间的偏差
for k=1:N
    Err_Messure(k)=abs(Z(k)-X(k));
    Err_Kalman(k)=abs(Xkf(k)-X(k));
end
```

## 运行结果

![image-20210917220451162](https://gitee.com/uestc-eagles/img/raw/master/images_typroa/image-20210917220451162.png)



![image-20210917220520430](https://gitee.com/uestc-eagles/img/raw/master/images_typroa/image-20210917220520430.png)

根据运行结果可以看出，运用kalman算法可以有效减小测量误差！！

